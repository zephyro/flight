<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Стоимость обучения</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->


        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Преподаватели</h1>

                <ul class="teachers">

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_01.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_00.png" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Ананко Лариса Александровна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_02.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_03.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_04.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_05.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_06.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_01.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_00.png" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Ананко Лариса Александровна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_02.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_03.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_04.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_05.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                    <li>
                        <a href="#" class="teachers-item">
                            <div class="teachers-image">
                                <img src="images/teachers/teacher_06.jpg" class="img-responsive" alt="">
                            </div>
                            <div class="teachers-name">
                                <span>Абрамова Наталья Михайловна</span>
                            </div>
                        </a>
                    </li>

                </ul>


            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

