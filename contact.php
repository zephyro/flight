<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Контакты</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Контакты</h1>

                <ul class="contact">
                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">МОСКВА</div>
                            <div class="contact-body">
                                <p>Шереметьевское ш., вл.6, УАТЦ МГТУ ГА,
                                    №851 м.Речной вокзал, №817 м.Планерная
                                    эт.3, оф.306 </p>
                                <p><a href="tel:">+7 (495) 768-88-09</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">МОСКВА</div>
                            <div class="contact-body">
                                <p>метро Полежаевская, Проспект
                                    Маршала Жукова,дом 2, 3 этаж, офис 85</p>
                                <p><a href="tel:+7 (499) 946-33-56<">+7 (499) 946-33-56</a>, <a href="tel:+7 (495) 589-13-50">+7 (499) 946-33-56</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">САНКТ-ПЕТЕРБУРГ</div>
                            <div class="contact-body">
                                <p>ул. Бухарестская, дом 24, корп.1</p>
                                <p><a href="tel:+7 (495) 768-88-09">+7 (495) 768-88-09</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">КАЗАНЬ</div>
                            <div class="contact-body">
                                <p>ул. Бухарестская, дом 24, корп.1</p>
                                <p><a href="tel:+7 (812) 927-35-53">+7 (812) 927-35-53</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">Волгоград</div>
                            <div class="contact-body">
                                <p>ул. Коммунистическая, дом 28А, офис 308</p>
                                <p><a href="tel:+7 (844) 298-87-76">+7 (844) 298-87-76</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">КРАСНОДАР</div>
                            <div class="contact-body">
                                <p>ул. Промышленная, дом 33, офис 65</p>
                                <p><a href="tel:+7 (861) 240-87-97">+7 (861) 240-87-97</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="contact-item">
                            <div class="contact-heading">КРАСНОЯРСК</div>
                            <div class="contact-body">
                                <p>ул. Мате Залки, дом 11, офис 208</p>
                                <p><a href="tel:+7 (391) 208-37-57">+7 (391) 208-37-57</a></p>
                            </div>
                            <div class="contact-bottom">
                                <a href="#">Как добраться</a>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

