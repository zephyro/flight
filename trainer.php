<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Тренажеры</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Тренажеры</h1>

                <ul class="trainer">
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_01.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>Трапы</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_02.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>Boeing 737</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_03.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>Boeing 747</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_04.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>Boeing 757</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_05.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>Boeing 767</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_06.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>AirBus 318/319/320/321</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_07.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>CRJ-100/200</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="trainer-item">
                            <div class="trainer-image">
                                <img src="images/trainer/trainer_08.jpeg" class="img-responsive" alt="">
                            </div>
                            <div class="trainer-name">
                                <span>ATR 42/72</span>
                            </div>
                        </a>
                    </li>
                </ul>


            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

