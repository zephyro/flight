<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Boeing 737</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Boeing 737</h1>

                <ul class="trainer-gallery">
                    <li>
                        <a href="images/trainer/trainer_01.jpeg" data-fancybox="gallery">
                            <img src="images/trainer/trainer_01.jpeg" class="img-responsive" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="images/trainer/trainer_02.jpeg" data-fancybox="gallery">
                            <img src="images/trainer/trainer_02.jpeg" class="img-responsive" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="images/trainer/trainer_03.jpeg" data-fancybox="gallery">
                            <img src="images/trainer/trainer_03.jpeg" class="img-responsive" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="images/trainer/trainer_04.jpeg" data-fancybox="gallery">
                            <img src="images/trainer/trainer_04.jpeg" class="img-responsive" alt="">
                        </a>
                    </li>
                </ul>

                <div class="text-center">
                    <a href="#" class="link-back"><i class="fa fa-caret-left"></i> <span>Вернуться назад</span></a>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

