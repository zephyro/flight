<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Программы обучения</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Программы обучения</h1>

                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_01.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Первоначальная подготовка бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Первоначальная подготовка бортпроводников для выполнения внутренних и международных полётов на ВС, включающая модуль "Учебная практика".</p>
                                <p>
                                    Стоимость обучения:<br/>
                                    <strong>60 000</strong> тыс. рублей <strong>с учетом практики</strong>.
                                </p>
                                <p>Объем учебной программы: 44 учебных дня</p>

                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_02.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Учебная наземная практика</span></a></h4>
                            <div class="program-text">
                                <p>Учебная практика на один тип ВС, ранее окончивших программу профессиональной подготовки по курсу «Первоначальная подготовка бортпроводников для выполнения внутренних и международных полетов» в ЧУ ДПО «Школа бортпроводников»</p>
                                <p>Стоимость обучения: <strong>20 000 тыс. рублей</strong>.</p>
                                <p>Объем учебной программы: 4 учебных дня</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_03.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Повышение квалификации бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Подготовка действующих бортпроводников по курсу повышения квалификации бортпроводников для выполнения внутренних и международных полётов на ВС.</p>
                                <p>Объем учебной программы: 10 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_02.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Переподготовка бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Профессиональная переподготовка действующих бортпроводников для выполнения полётов на ВС.</p>
                                <p>Объем учебной программы: 1-5 учебный дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_03.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Подготовка старших бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Обучение бортпроводников по курсу первоначальной подготовки/повышение квалификации старших бортпроводников кабинного экипажа воздушных судов.</p>
                                <p>Объем учебной программы: 2-9 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_01.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Подготовка инструкторов бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Обучении бортпроводников по курсу первоначальной подготовки/повышение квалификации инструкторов проводников бортовых.</p>
                                <p>Объем учебной программы: 6-12 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_01.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Первоначальная подготовка бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Первоначальная подготовка бортпроводников для выполнения внутренних и международных полётов на ВС, включающая модуль "Учебная практика".</p>
                                <p>
                                    Стоимость обучения:<br/>
                                    <strong>60 000</strong> тыс. рублей <strong>с учетом практики</strong>.
                                </p>
                                <p>Объем учебной программы: 44 учебных дня</p>

                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_02.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Учебная наземная практика</span></a></h4>
                            <div class="program-text">
                                <p>Учебная практика на один тип ВС, ранее окончивших программу профессиональной подготовки по курсу «Первоначальная подготовка бортпроводников для выполнения внутренних и международных полетов» в ЧУ ДПО «Школа бортпроводников»</p>
                                <p>Стоимость обучения: <strong>20 000 тыс. рублей</strong>.</p>
                                <p>Объем учебной программы: 4 учебных дня</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_03.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Повышение квалификации бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Подготовка действующих бортпроводников по курсу повышения квалификации бортпроводников для выполнения внутренних и международных полётов на ВС.</p>
                                <p>Объем учебной программы: 10 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_01.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Первоначальная подготовка бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Первоначальная подготовка бортпроводников для выполнения внутренних и международных полётов на ВС, включающая модуль "Учебная практика".</p>
                                <p>
                                    Стоимость обучения:<br/>
                                    <strong>60 000</strong> тыс. рублей <strong>с учетом практики</strong>.
                                </p>
                                <p>Объем учебной программы: 44 учебных дня</p>

                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_02.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Учебная наземная практика</span></a></h4>
                            <div class="program-text">
                                <p>Учебная практика на один тип ВС, ранее окончивших программу профессиональной подготовки по курсу «Первоначальная подготовка бортпроводников для выполнения внутренних и международных полетов» в ЧУ ДПО «Школа бортпроводников»</p>
                                <p>Стоимость обучения: <strong>20 000 тыс. рублей</strong>.</p>
                                <p>Объем учебной программы: 4 учебных дня</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_03.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Повышение квалификации бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Подготовка действующих бортпроводников по курсу повышения квалификации бортпроводников для выполнения внутренних и международных полётов на ВС.</p>
                                <p>Объем учебной программы: 10 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

