<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Новости</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Новости</h1>

                <div class="news-list">

                    <div class="news">
                        <h4 class="news-title">
                            <a href="#"><span class="news-date">24.07.2017</span> ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» награждена Дипломом</a>
                        </h4>
                        <div class="news-intro">
                            <div class="news-intro-image">
                                <a href="#">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="news-introtext">ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» наградили Дипломом за высокий профессионализм в организации ярмарки вакансий</div>
                        </div>
                    </div>

                    <div class="news">
                        <h4 class="news-title">
                            <a href="#"><span class="news-date">24.07.2017</span> ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» награждена Дипломом</a>
                        </h4>
                        <div class="news-intro">
                            <div class="news-intro-image">
                                <a href="#">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="news-introtext">ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» наградили Дипломом за высокий профессионализм в организации ярмарки вакансий</div>
                        </div>
                    </div>

                    <div class="news">
                        <h4 class="news-title">
                            <a href="#"><span class="news-date">24.07.2017</span> ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» награждена Дипломом</a>
                        </h4>
                        <div class="news-intro">
                            <div class="news-intro-image">
                                <a href="#">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="news-introtext">ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» наградили Дипломом за высокий профессионализм в организации ярмарки вакансий</div>
                        </div>
                    </div>

                    <div class="news">
                        <h4 class="news-title">
                            <a href="#"><span class="news-date">24.07.2017</span> ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» награждена Дипломом</a>
                        </h4>
                        <div class="news-intro">
                            <div class="news-intro-image">
                                <a href="#">
                                    <img src="images/news_image.jpg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="news-introtext">ЧУ ДПО «ШКОЛА БОРТПРОВОДНИКОВ» наградили Дипломом за высокий профессионализм в организации ярмарки вакансий</div>
                        </div>
                    </div>

                </div>

                <ul class="pagination">
                    <li><a href="#">1</a></li>
                    <li class="active"><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><span>.....</span></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">9</a></li>
                </ul>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

