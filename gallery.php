<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Фотогалерея</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Фотогалерея</h1>

                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        <div class="gallery-nav">
                            <div class="gallery-nav-heading">
                                <span class="gallery-toggle" data-target=".gallery-menu">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>

                                <span class="navbar-title">Разделы</span>

                            </div>

                            <ul class="gallery-menu">
                                <li class="active"><a href="#">Наши выпускники</a></li>
                                <li><a href="#">Учебная практика</a></li>
                                <li><a href="#">Пожарный тренажер</a></li>
                                <li><a href="#">Тренажерная подготовка</a></li>
                                <li><a href="#">Водная подготовка</a></li>
                                <li><a href="#">Занятия в классе</a></li>
                                <li><a href="#">Участие в мероприятиях</a></li>
                                <li>
                                    <a href="#">Международный практический семинар 2015</a>
                                    <ul>
                                        <li><a href="#">Регистрация участников </a></li>
                                        <li><a href="#">Выступающие</a></li>
                                        <li><a href="#">Работа семинара</a></li>
                                        <li><a href="#">Дефиле</a></li>
                                        <li><a href="#">Поздравления с юбилеем школы от авиакомпаний</a></li>
                                        <li><a href="#">Поздравления с юбилеем школы от учеников</a></li>
                                        <li><a href="#">Общие фотографии</a></li>
                                    </ul>
                                </li>

                            </ul>

                        </div>


                    </div>
                    <div class="col-md-8 col-lg-9">
                        <div class="gallery">
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_01.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_01.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_02.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_02.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_03.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_03.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_04.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_04.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_05.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_05.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_06.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_06.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_07.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_07.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_08.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_08.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_09.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_09.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                            <div class="gallery-item">
                                <a href="images/gallery/gallery_10.jpeg" data-fancybox="gallery">
                                    <img src="images/gallery/gallery_10.jpeg" class="img-responsive" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

