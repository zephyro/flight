$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
        pull.toggleClass('open');
    });
});

$(function() {
    var pull = $('.gallery-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.toggleClass('open');
        pull.toggleClass('open');
    });
});

$('.partners').slick({
    dots: false,
    infinite: false,
    autoplay: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: '<span class="slide-nav prev"><i class="fa fa-angle-left"></i></span>',
    nextArrow: '<span class="slide-nav next"><i class="fa fa-angle-right"></i></span>',
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 3
            }
        },
        {
            breakpoint: 768,
            settings: {
                infinite: false,
                slidesToShow: 2
            }
        }
    ]
});
