<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Главная</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="programs-block">
            <div class="container">
                <div class="h1">ЧУ ДПО «Школа бортпроводников»</div>
                <div class="row">
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_01.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Первоначальная подготовка бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Первоначальная подготовка бортпроводников для выполнения внутренних и международных полётов на ВС, включающая модуль "Учебная практика".</p>
                                <p>
                                    Стоимость обучения:<br/>
                                    <strong>60 000</strong> тыс. рублей <strong>с учетом практики</strong>.
                                </p>
                                <p>Объем учебной программы: 44 учебных дня</p>

                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_02.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Учебная наземная практика</span></a></h4>
                            <div class="program-text">
                                <p>Учебная практика на один тип ВС, ранее окончивших программу профессиональной подготовки по курсу «Первоначальная подготовка бортпроводников для выполнения внутренних и международных полетов» в ЧУ ДПО «Школа бортпроводников»</p>
                                <p>Стоимость обучения: <strong>20 000 тыс. рублей</strong>.</p>
                                <p>Объем учебной программы: 4 учебных дня</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_03.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Повышение квалификации бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Подготовка действующих бортпроводников по курсу повышения квалификации бортпроводников для выполнения внутренних и международных полётов на ВС.</p>
                                <p>Объем учебной программы: 10 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_02.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Переподготовка бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Профессиональная переподготовка действующих бортпроводников для выполнения полётов на ВС.</p>
                                <p>Объем учебной программы: 1-5 учебный дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_03.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Подготовка старших бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Обучение бортпроводников по курсу первоначальной подготовки/повышение квалификации старших бортпроводников кабинного экипажа воздушных судов.</p>
                                <p>Объем учебной программы: 2-9 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4">
                        <div class="program-item">
                            <a href="#" class="program-image">
                                <img src="images/news_img_01.jpg" class="img-responsive" alt="">
                            </a>
                            <h4><a href="#"><span>Подготовка инструкторов бортпроводников</span></a></h4>
                            <div class="program-text">
                                <p>Обучении бортпроводников по курсу первоначальной подготовки/повышение квалификации инструкторов проводников бортовых.</p>
                                <p>Объем учебной программы: 6-12 учебных дней</p>
                            </div>
                            <div class="program-bottom">
                                <a href="#">Подробнее о программе</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="text-center">
                    <a href="#" class="btn">Другие виды обучения</a>
                </div>
            </div>
        </section>

        <section class="blue-block">
            <div class="container">
                <p>
                    Мы рады приветствовать Вас на официальном сайте ЧУ ДПО "Школа бортпроводников"! За 20 лет успешной работы наш учебный центр подготовил тысячи высококвалифицированных специалистов,работающих как в российских, так и в зарубежных авиакомпаниях. Мы видим своей главной задачей ответственную плодотворную работу по подготовке востребованных авиаспециалистов.
                </p>
                <div class="video-block">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe src="https://www.youtube.com/embed/An0AP0-smqg" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>

        <section class="white-block">
            <div class="container">
                <div class="training-set">
                    <div class="training-title">Следующий набор в школу</div>
                    <div class="text-center">
                        <a href="#order" class="btn btn-modal">Записаться</a>
                    </div>
                    <div class="training-info">
                        <p>Обучение очное,</p>
                        <p>время занятий с <span>9:30</span> до <span>16:00</span></p>
                        <p>каждый день кроме воскресенья</p>
                    </div>
                    <a href="#" class="training-link">РАСПИСАНИЕ ВСЕХ КУРСОВ</a>
                </div>
            </div>
        </section>

        <section class="promo-block">
            <div class="container">
                <div class="h2">Почему мы?</div>
                <div class="row">
                    <div class="col-sm-6">
                        <ul>
                            <li>20 лет успешной деятельности</li>
                            <li>Современная учебно-тренажерная база</li>
                            <li>Доступные цены на обучение</li>
                            <li>Проведение учебной практики на макете воздушного судна под руководством преподавателей-инструкторов и получение пакета документов, позволяющего получить Свидетельство бортпроводника</li>
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <ul>
                            <li>Возможность обучения на английском языке</li>
                            <li>Осуществляем подготовку более чем на 30 типов воздушных судов</li>
                            <li>Интересная профессия всего за 2 месяца обучения</li>
                            <li>Содействие в трудоустройстве</li>
                            <li>Возможность обучения в других городах России</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="review-block">
            <div class="container">
                <div class="h2">Отзывы</div>
                <ul class="review">
                    <li>
                        <div class="review-image">
                            <img src="images/review_02.jpg" class="img-responsive">
                        </div>
                        <div class="review-text">
                            <div class="review-author">
                                <strong>Анна Иванова</strong> <span>Выпускница</span>
                            </div>
                            <p>С трудоустройством школа старается помочь,особенно если хорошо зарекомендуешь себя во время занятий.Я устроился в Аэрофлот-Дон (сейчас Донавиа).Там проходил стажировку и потом мне предложили остаться. Главное не лениться.Я фанат своей работы,поэтому на рейсы приходил в отличном настроении и с желанием работать. Обо всех же своих одногруппниках не знаю,но у тех,с кем общаюсь с работой всё нормально.»</p>
                        </div>
                    </li>
                    <li>
                        <div class="review-image">
                            <img src="images/review_01.jpg" class="img-responsive">
                        </div>
                        <div class="review-text">
                            <div class="review-author">
                                <strong>Сергей Смирнов</strong> <span>Выпускник</span>
                            </div>
                            <p>«С трудоустройством школа старается помочь,особенно если хорошо зарекомендуешь себя во время занятий.Я устроился в Аэрофлот-Дон (сейчас Донавиа).Там проходил стажировку и потом мне предложили остаться. Главное не лениться.Я фанат своей работы,поэтому на рейсы приходил в отличном настроении и с желанием работать. Обо всех же своих одногруппниках не знаю,но у тех,с кем общаюсь с работой всё нормально.»</p>
                        </div>
                    </li>
                </ul>

                <div class="text-center">
                    <a href="#" class="btn btn-review">Все отзывы</a>
                </div>
            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

