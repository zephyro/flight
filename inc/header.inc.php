<header class="header">
    <div class="container">
        <a href="#" class="header-logo">
            <img src="img/logo.png" class="img-responsive" alt="">
        </a>
        <div class="clearfix">
            <ul class="header-small-nav">
                <li><a href="#" class="bvi-panel-open"><i class="fa fa-eye" title="Версия для слабовидящих"></i></a></li>
                <li><a href="#" class="lnd-en">Еn</a></li>
                <li><a href="#"><i class="fa fa-vk"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
            </ul>
            <a href="#" class="header-tel"><span>+7 (495)</span> 768-88-09</a>
            <div class="header-nav">
                    <span class="navbar-toggle" data-target=".nav">
                        <span></span>
                        <span></span>
                        <span></span>
                    </span>
                <ul class="nav">
                    <li class="nav-item-1"><a href="#">О школе</a></li>
                    <li class="nav-item-2"><a href="#">Программы обучения</a></li>
                    <li class="nav-item-3"><a href="#">Стоимость обучения</a></li>
                    <li class="nav-item-4"><a href="#">Электронное обучение</a></li>
                    <li class="nav-item-5"><a href="#">Форум</a></li>
                    <li class="nav-item-6"><a href="#">Контакты</a></li>
                </ul>
            </div>
        </div>
        <div  class="header-office">
            <a href="#">Наши филиалы</a>
            <span>в 8 городах России</span>
        </div>

    </div>
</header>