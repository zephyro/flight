<footer class="footer">
    <div class="container">

        <div class="footer-logo">
            <img src="img/logo.png" class="img-responsive" alt="">
        </div>

        <div class="footer-top">

            <div class="footer-social">
                <div class="footer-title">Мы в соц. сетях:</div>
                <ul>
                    <li><a href="#"><i class="fa fa-vk"></i> <span>Вконтакте</span></a></li>
                    <li><a href="#"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>
                    <li><a href="#"><i class="fa fa-instagram"></i> <span>instagram</span></a></li>
                </ul>
            </div>

            <div class="footer-contact">
                <ul class="footer-contact-top">
                    <li><a href="tel:+7 (495) 768-88-09">+7 (495) 768-88-09 </a></li>
                    <li><a href="mailto:info@airschool.ru">info@airschool.ru</a></li>
                </ul>
                <p>Адрес:  Москва, метро Полежаевская, пр-т Маршала Жукова, 2, 3 этаж, офис 85</p>
            </div>

        </div>

        <div class="footer-branch">
            <div class="footer-title">ФИЛИАЛЫ</div>
            <ul>
                <li><a href="#">Москва</a></li>
                <li><a href="#">Самара</a></li>
                <li><a href="#">Казань</a></li>
                <li><a href="#">Санкт-петербург</a></li>
                <li><a href="#">Волгоград</a></li>
                <li><a href="#">Краснодар</a></li>
                <li><a href="#">Красноярск</a></li>
                <li><a href="#">УФА</a></li>
            </ul>
        </div>

        <div class="footer-nav">
            <div class="footer-title">МЕНЮ</div>
            <ul>
                <li><a href="#">Программы обучения</a></li>
                <li><a href="#">Электронное обучение</a></li>
                <li><a href="#">Форум</a></li>
                <li><a href="#">Стоимость обучения</a></li>
            </ul>
            <ul>
                <li><a href="#">Новости</a></li>
                <li><a href="#">Тренажеры</a></li>
                <li><a href="#">Преподаватели</a></li>
                <li><a href="#">Документы АУЦ</a></li>
                <li><a href="#">Отзывы</a></li>
                <li><a href="#">Фотогалерея</a></li>
            </ul>
        </div>

        <div class="footer-bottom">© 2001 "Школа бортпроводников"</div>

    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.1.1.min.js"><\/script>')</script>
<script src="js/vendor/jquery.fancybox/jquery.fancybox.min.js"></script>
<script src="js/vendor/jquery.scrollTo.min.js"></script>
<script src="js/vendor/slick/slick.min.js"></script>

<script src="button-visually-impaired/js/responsivevoice.min.js?ver=1.5.0"></script>
<script src="button-visually-impaired/js/bvi.min.js"></script>
<script src="button-visually-impaired/js/js.cookie.js"></script>
<script src="button-visually-impaired/js/bvi-init-panel.js"></script>

<script src="js/main.js"></script>