/*!
 * Button visually impaired v1.0.6
 */

$(document).ready(function($) {
    $('.bvi-panel-open').bvi('Init', {"BviPanel" : "1", "BviPanelBg" : "white", "BviPanelFontSize" : "18"});
});
