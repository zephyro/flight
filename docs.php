<!doctype html>
<html class="no-js" lang="ru">
    <head>

        <title>Новости</title>

        <!-- Head -->
        <?php include('inc/head.inc.php') ?>
        <!-- -->

    </head>
    
    <body>

        <!-- Header -->
        <?php include('inc/header.inc.php') ?>
        <!-- -->

        <section class="main">
            <div class="container">

                <!-- TopNav -->
                <?php include('inc/topnav.inc.php') ?>
                <!-- -->

                <h1 class="text-center">Документы АУЦ</h1>

                <ul class="docs">
                    <li><a href="#"><span>Лицензия НОУ Школа бортпроводников S</span></a></li>
                    <li><a href="#"><span>Сертификат АУЦ №39 S</span></a></li>
                    <li><a href="#"><span>Устав НОУ Школа бортпроводников S</span></a></li>
                    <li><a href="#"><span>Правила внутреннего трудового распорядка S</span></a></li>
                    <li><a href="#"><span>Типовой Договор юридические лица S</span></a></li>
                    <li><a href="#"><span>Типовое Соглашение физические лица S</span></a></li>
                    <li><a href="#"><span>Положение по Обеспечению Качества S</span></a></li>
                    <li><a href="#"><span>Руководство по организации учебного процесса S</span></a></li>
                    <li><a href="#"><span>Сведения о персональном составе S</span></a></li>
                    <li><a href="#"><span>Сведения о материально-техническом обеспечении S</span></a></li>
                    <li><a href="#"><span>Структура программ S</span></a></li>
                    <li><a href="#"><span>Отчет о результатах самообследования S</span></a></li>
                    <li><a href="#"><span>Список руководителей S</span></a></li>
                </ul>

            </div>
        </section>

        <!-- Footer -->
        <?php include('inc/partners.inc.php') ?>
        <!-- -->

        <!-- Footer -->
        <?php include('inc/footer.inc.php') ?>
        <!-- -->

    </body>
</html>

